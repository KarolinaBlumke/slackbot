FROM python:2.7
ADD . /slackbot
WORKDIR /slackbot
RUN pip install -r bots/requirements.txt
