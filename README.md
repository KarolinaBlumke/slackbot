# Slackbot #

Instruction how to set up and use Slackbot.

### Bitbucket repository ###

* https://bitbucket.org/KarolinaBlumke/slackbot

### How do I get set up? ###

Run:

* `docker-compose build`
* `docker-compose up`

In another terminal type:

* `docker ps` - to find the multiplexer container id
* `docker attach <multiplexer-container-id>`

* in this terminal window you can communicate with bots:

    * type `list` to get list of available bots
    * type `start_session <bot_name>` to start chatting with bot
    * type `end_session` to stop connection with given bot
