import pytest  # noqa

from bots.bot import BOTS, PORTS


def test_if_bots_are_registered(register_bots):
    """Check if all defined Bots are registered."""
    assert BOTS == {
        'Agent Smith': 'http://bot3:8003/askmeanything/<string:question>',
        'HAL 9000': 'http://bot1:8001/askmeanything/<string:question>',
        'Wall-E': 'http://bot2:8002/askmeanything/<string:question>'
    }


def test_if_ports_are_defined(register_bots):
    """Check if ports for bots are defined"""
    assert PORTS == {
        'Agent Smith': 8003,
        'HAL 9000': 8001,
        'Wall-E': 8002
    }
