"""Pytest fixtures."""
import pytest

from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

from bots import hal9000, wall_e, agent_smith
from bots.bot import BOTS, PORTS, MainBot


class TestBot(MainBot):

    NAME = 'TEST'
    PORT = 11112

    def __init__(self, bot):
        super(TestBot, self).__init__(bot)
        self.bot = ChatBot(
            self.NAME,
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database_uri='mongodb://test:11111/',
            database=self.NAME.replace(' ', '-')
        )
        self.bot.set_trainer(ChatterBotCorpusTrainer)
        self.bot.train("chatterbot.corpus.english")


# def create_app(name):
#     app = Flask(name)
#     return app

# # bot fixture
# @pytest.fixture
# def bot(TestBot, mongodb, app):
#     create_app('Test')
#     # TestBot.register()
#     # TestBot.add_endpoint(test)
#     # return TestBot.run(port=test.PORT, host='0.0.0.0', debug=True)


# Register defined bots.
@pytest.fixture
def register_bots():
    hal9000.HAL_9000.register()
    wall_e.Wall_e.register()
    agent_smith.Agent_Smith.register()
