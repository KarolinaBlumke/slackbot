import pytest  #noqa

import __builtin__

from bots import hal9000, wall_e, agent_smith
from bots.bot import BOTS, PORTS

from multiplexer.multiplexer import (
    return_bots_list, check_if_bot_exsist, get_answer)


def test_return_bots_list(register_bots):
    'Check if function return properly forrmatted Bots list.'
    expected = """1: Wall-E\n2: Agent Smith\n3: HAL 9000"""
    assert return_bots_list(BOTS) == expected


@pytest.mark.parametrize("bot_name, expected", [
    ("Wall-E", True),
    ("Agent Smith", True),
    ("HAL 9000", True),
    ("", False),
    ("Thomson", False),
])
def test_if_bot_exsist(register_bots, bot_name, expected):
    'Check if bots of given name exsist.'
    assert check_if_bot_exsist(bot_name, BOTS) == expected


# def test_get_answer(register_bots, monkeypatch):
#     """Get user input without actually having a
#     user type letters using monkeypatch. """
#     def mock_raw_input(*args, **kwargs):
#         """ Act like someone just typed 'hello'. """
#         return 'Hello'

#     # Put the mock_raw_input in place of the actual raw_input on the
#     # __builtin__ module.
#     monkeypatch.setattr(__builtin__, 'raw_input', mock_raw_input)

#     # retval should now contain 'Hello'
#     cmd = raw_input()

#     assert cmd == 'Hello'
#     assert get_answer('Test', cmd) == ''
