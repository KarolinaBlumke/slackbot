from flask import Flask

from bot import MainBot

hal9000 = Flask('Hal9000')


class HAL_9000(MainBot):

    NAME = 'HAL 9000'
    PORT = 8001
    BOT_NUMBER = 1



HAL_9000.register()

if __name__ == '__main__':
    HAL_9000.add_endpoint(hal9000)
    hal9000.run(port=HAL_9000.PORT, host='0.0.0.0', debug=True)
