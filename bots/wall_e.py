from flask import Flask

from bot import MainBot

wall_e = Flask('Wall-E')


class Wall_e(MainBot):

    NAME = 'Wall-E'
    PORT = 8002
    BOT_NUMBER = 2


Wall_e.register()

if __name__ == '__main__':
    Wall_e.add_endpoint(wall_e)
    wall_e.run(port=Wall_e.PORT, host='0.0.0.0', debug=True)
