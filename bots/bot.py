"""Main class for a bot definition."""
from flask import jsonify

from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

BOTS = {}
PORTS = {}


class MainBot(object):

    NAME = ''
    PORT = 0
    BOT_NUMBER = 0
    PATH = '/askmeanything/<string:question>'

    def __init__(self):

        self.bot = ChatBot(
            self.NAME,
            storage_adapter="chatterbot.storage.MongoDatabaseAdapter",
            database_uri='mongodb://db:27017/',
            database=self.NAME.replace(' ', '-')

        )
        self.bot.set_trainer(ChatterBotCorpusTrainer)
        self.bot.train("chatterbot.corpus.english")

    @classmethod
    def register(cls):
        cls.ENDPOINT = 'http://bot' + str(cls.BOT_NUMBER) + ':' + str(cls.PORT) + cls.PATH
        BOTS[cls.NAME] = cls.ENDPOINT
        PORTS[cls.NAME] = cls.PORT

    @classmethod
    def add_endpoint(cls, app):
        bot = cls()

        def get_raw_response(question):
            return jsonify(bot.bot.get_response(question).text)
        get_raw_response.__name__ = cls.NAME
        app.route(cls.PATH, methods=['GET'])(get_raw_response)
