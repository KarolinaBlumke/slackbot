from flask import Flask

from bot import MainBot

agent_smith = Flask('agent_smith')


class Agent_Smith(MainBot):
    """"""
    NAME = 'Agent Smith'
    PORT = 8003
    BOT_NUMBER = 3


Agent_Smith.register()

if __name__ == '__main__':
    Agent_Smith.add_endpoint(agent_smith)
    agent_smith.run(port=Agent_Smith.PORT, host='0.0.0.0', debug=True)
