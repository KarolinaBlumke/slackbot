"""VCOS - Virtual Chatbots Operator Service."""

import requests

from flask import Flask, jsonify

from bots import hal9000, wall_e, agent_smith
from bots.bot import BOTS, PORTS

multiplexer = Flask(__name__)


bot = None


def return_bots_list(dic):
    """Print list of exsisting bots."""
    bots = dict(zip(range(1,len(dic.keys())+1), dic.keys()))
    bots_list = []
    for k, v in bots.iteritems():
                bots_list.append('%d: %s' % (k, v))
    bots_list =  '\n'.join(bots_list)
    return bots_list


def check_if_bot_exsist(bot_name, bots):
    """Check if bots exsists."""
    if bot_name in bots:
        print 'You are now chatting with {}'.format(bot_name)
        return True
    else:
        print "Sorry, not available. Please type 'list' to see available bots"
        return False

def get_answer(bot, cmd):
    """Get answer for given bot."""
    bot_endpoint = BOTS[bot]
    bot_port = PORTS[bot]
    answer =  bot + ': ' + str(requests.get(bot_endpoint.replace('<string:question>', cmd)).text)
    print answer
    return answer

if __name__ == '__main__':
    while True:
        cmd = raw_input()
        if not bot:
            if cmd == 'list':
                print return_bots_list(BOTS)
            elif cmd.startswith('start_session '):
                bot = cmd.split(' ', 1)[1]
                if not check_if_bot_exsist(bot, BOTS):
                    bot = None
            else:
                print 'Invalid command.'
        else:
            if cmd == 'end_session':
                bot = None
            else:
                get_answer(bot, cmd)
