import time
from bots.bot import BOTS, PORTS
from bots import hal9000, wall_e, agent_smith
from multiplexer import return_bots_list, get_answer

from config import SLACK_BOT_TOKEN, BOT_ID
from slackclient import SlackClient

# constants
AT_BOT = "<@" + BOT_ID + ">"

# instantiate Slack client
slack_client = SlackClient(SLACK_BOT_TOKEN)

bot = None

def check_if_bot_exsist(bot_name, bots):
    """Check if bots exsists."""
    if bot_name in bots:
        return 'You are now chatting with {}'.format(bot_name)
    else:
        return "Sorry, not available. Please type 'list' to see available bots"


def handle_command(command, channel):
    """
    Receives commands directed at the bot and determines if they
    are valid commands. If so, then acts on the commands. If not,
    returns back what it needs for clarification.
    """
    global bot
    if not bot:
        if command == 'list':
            response = return_bots_list(BOTS)
        elif command.startswith('start_session '):
            bot = command.split(' ', 1)[1]
            response = check_if_bot_exsist(bot, BOTS)
        else:
            response = 'Invalid command.'
    else:
        if command == 'end_session':
            bot = None
            response = 'Session ended. Type list to get list of bots.'
        else:
            response = get_answer(bot, command)
    slack_client.api_call("chat.postMessage", channel=channel,
                          text=response, as_user=True)


def parse_slack_output(slack_rtm_output):
    """
    Parse slack output.
    """
    output_list = slack_rtm_output
    if output_list and len(output_list) > 0:
        for output in output_list:
            if output and 'text' in output and AT_BOT in output['text']:
                # return text after the @ mention, whitespace removed
                return output['text'].split(AT_BOT)[1].strip(), \
                       output['channel']
    return None, None


if __name__ == "__main__":
    READ_WEBSOCKET_DELAY = 1  # 1 second delay between reading from firehose
    if slack_client.rtm_connect():
        print("chatterbot connected and running!")
        while True:
            command, channel = parse_slack_output(slack_client.rtm_read())
            if command and channel:
                handle_command(command, channel)
            time.sleep(READ_WEBSOCKET_DELAY)
    else:
        print("Connection failed. Invalid Slack token or bot ID?")
